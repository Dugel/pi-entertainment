#! /bin/bash
sudo apt-get install python3 -y
sudo apt-get install python3-tk -y
sudo apt-get install git -y
sudo apt-get install python3-pip -y
sudo apt-get install python3-pyqt5.qtwebkit -y
pip3 install PyQtWebEngine
python3 -m pip install PyQt5
su - pi
cd ~
git clone https://gitlab.com/Dugel/pi-entertainment.git
sudo sed -i 's/@lxpanel/#@lxpanel/g' /etc/xdg/lxsession/LXDE-pi/autostart
echo "alias ll='ls -la --color=auto'" >> ~/.bashrc
export DISPLAY=:0.0
/usr/bin/python3 ~/pi-entertainment/main.py
