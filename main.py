import tkinter as tk   
import sys, webbrowser, os
from PyQt5.Qt import *


def syno_browser():
    url = "http://192.168.1.121/photo/m/#!Albums"
    chrome_path = '/usr/lib/chromium-browser/chromium-browser'
    webbrowser.get(chrome_path).open(url)

def write_slogan():
    print("Tkinter is easy to use!")

def access_photo():
    print("Ouverture de l'accès aux Photos")

def main():
    root = tk.Tk()
    root.geometry("800x600")  
    root.title("Manon divertissements")
    root.attributes('-fullscreen', True)
    frame = tk.Frame(root)
    frame.pack()

    quitbutton = tk.Button(frame, 
                   text="QUIT", 
                   fg="red",
                   command=quit)

    testbutton = tk.Button(frame,
                   text="MUSIQUE",
                   fg="green",
                   command=syno_browser)

    photobutton = tk.Button(frame,
                   text="PHOTO",
                   fg="blue",
                   command=syno_browser)

    quitbutton.pack(side=tk.LEFT)
    testbutton.pack(side=tk.LEFT)
    photobutton.pack(side=tk.LEFT)

    root.mainloop()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    main()
